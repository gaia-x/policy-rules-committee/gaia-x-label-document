# List of Gaia-X Conformity Assessment Bodies


All Gaia-X Conformity Assessment Bodies (CABs) which are accredited to attestate conformity against a permissable standard by a respective standards organizations body are accepted by Gaia-X.

[Accreditation Bodies: ISO standard definition here](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.7)

As of 06/06/2024, the list of the accepted [accreditation bodies](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.7) is located at <https://www.iafcertsearch.org/search/accreditation-bodies>

## SecNumCloud

The list of official assessment bodies for SecNumCloud is located at <https://cyber.gouv.fr/voir-les-centres-devaluation>.

As of 28/05/2024, the list is the following one:

- [AFNOR Certification](www.afnor.org)
- [International Certification Trust Services (ICTS)](www.certi-trust.com)
- [LNE](www.lne.fr)
- [LSTI](www.lsti-certification.fr)

## ISO 27001

The list of official certification bodies for ISO 27001 is located in [InternationalAccreditationForum](https://www.iafcertsearch.org/search/certification-bodies?standards_id=4cb16367-3cd5-5a6c-a382-1f524564b9d9&standards_name=ISO%2027001)

As of 07/06, 501 CAB are accepted for ISO 27001 worlwide by IAF.
They will not be listed in this document.
The uptodate list is in the [GAIA-X registry](https://registry.gaia-x.eu/).

!!! note

    In France the accredidation body is located at [COFRAC](https://www.cofrac.fr/).

As of 03/06/24 the list of COFRAC for France is:

- [AFNOR Certification](www.afnor.org)
- [International Certification Trust Services France](www.certi-trust.com)
- [LNE](www.lne.fr)
- [LSTI](www.lsti-certification.fr)
- [SGS International Certification Service](http://www.fr.sgs.com/)
- [Skill4All](http://www.bestcertifs.com/)
- [Vigicert](http://www.vigicert.com/)

## EU Cloud CoC

The list of official monitoring bodies able to deliver EU Cloud Code of Conduct assement is located at <https://eucoc.cloud/en/public-register/assessment-procedure>.

As of 03/06/24 the only one is:

- [SCOPE Europe](https://scope-europe.eu/en/monitoring-body)


## CISPE Code of Conduct
 
The list of official assessment bodies for CISPE is located at <https://www.codeofconduct.cloud/monitoring-bodies/>.
 
As of 10/06/2024, the list is the following one:
 
- [Bureau Veritas](group.bureauveritas.com)
- [EY CertifyPoint](https://www.ey.com/en_nl/consulting/certify-point)
- [LNE (Laboratoire national de métrologie et d’essais)](www.lne.fr/en)
 
 
## Cloud Security Alliance

The list of official CSA certified STAR auditors is located at : <https://cloudsecurityalliance.org/star/certified-star-auditors>

As of 11/06/2024, more than forty CAB are accepted for CSA STAR so they will not be listed in this document.
The up to date list is in the [GAIA-X registry](https://registry.gaia-x.eu/).

 ## BSI C5

As of 26/06/2024, BSI C5 doesn't provide the list of official assessment bodies authoristed to issue BSI C5 attestion : [BSI](https://www.bsi.bund.de/EN/Themen/Unternehmen-und-Organisationen/Informationen-und-Empfehlungen/Empfehlungen-nach-Angriffszielen/Cloud-Computing/Kriterienkatalog-C5/C5_Einfuehrung/Kunde/Kunde_node.html).
As confirmed by BSI on 3/07/2024: 
"The BSI often receives enquiries regarding who can perform a C5 audit and whether the BSI can recommend or arrange auditors. The BSI does not as a principle make any such recommendations. The requirements for auditors are specified in Chapter 3 of the C5 and adhering to them should be specified as part of the contract to appoint an auditor."
[SourceBSI](https://www.bsi.bund.de/EN/Themen/Unternehmen-und-Organisationen/Informationen-und-Empfehlungen/Empfehlungen-nach-Angriffszielen/Cloud-Computing/Kriterienkatalog-C5/C5_Einfuehrung/Pruefer/Pruefer_node.html) -- see the subsection "Recommending or appointing an auditor".

It was found in a personnal website published in an Enisa document [CyberSecurity Assessments ver Jan2024](https://www.enisa.europa.eu/publications/cybersecurity-market-assessments/@@download/fullReport)

As of 19/06/2024, 8 CAB are accepted for BSI C5 :

- [PwC Germany (DEU)](www.pwc.de/en/digitale-transformation/bsi-c5-german-bsi-catalogue-of-requirements-for-more-transparency-in-the-cloud.html)
- [HKKG (DEU)](hkkg-koeln.de/c5-pruefungen/)
- [EY (DEU)](www.ey.com/de_de/risk/attestation-certification-atc)
- [BDO Deutschland (DEU)](www.bdo.de/de-de/services/audit-assurance/it-controls-assurance/outsourcing-assurance)
- [Rödl & Partner (DEU)](www.roedl.de/wen-wir-beraten/gesundheit-sozialwirtschaft/c5-testat)
- [TÜV Nord Group (DEU)](www.tuev-nord-group.com/en/newsroom/news/details/article/secure-cloud-computing-according-to-c5-tuevit-has-formed-a-partnership-with-fides-treuhand/)
- [Schellmann (US)](www.schellman.com/blog/cloud-compliance/what-is-c5-attestation)
- [Lazarus Alliance (US)](lazarusalliance.com/services/audit-compliance/c5/)

The uptodate list is in the [GAIA-X registry](https://registry.gaia-x.eu/).

## SWIPO

As of the 13/06/2024, there is no CAB approved by [SWIPO](https://swipo.eu/).
You can find information regarding SWIPO at [SWIPO Certification document](https://swipo.eu/wp-content/uploads/2020/07/SWIPO-review-of-approaches-to-certification-schemes.pdf).

As there is no SWIPO CAB, SWIPO permissible standard can be used only in case of declaration. SWIPO permissible standard can't be used if certification is needed.

## Climate Neutral Data Center Pact

As of the 17/06/2024 the CNDC Pact is a self assessment. More information : [CNDC](https://www.climateneutraldatacentre.net/signatories/).

As CNDC Pact is a self assessment, CNDC permissible standard can be used only in case of declaration. CNDC Pact permissible standard can't be used if certification is needed.

## TISAX

The list of official TIXAS audit providers is located at <https://enx.com/en-US/TISAX/xap/>

As an example, the 11/06/2024 there were 15 assement bodies listed just in Germany. Worldwide accepted CAB will not be listed in this document. 
The up to date list is in the [GAIA-X registry](https://registry.gaia-x.eu/).
 

# Process description for how to become a Gaia-X compliant user

Assumed prerequisites:

1.	the user is already familiar with the concepts of Gaia-X, like the Verifiable Credential model (digital signatures/using certificates/digital wallets).

2.	the user has an EV SSL or an eIDAS certificate and the public part of the certificate is published via DID:WEB method.

3.	the user is familiar with the workflow described in the Architecture Document.

**A - The user wants to get Gaia-X Compliant Verifiable Credentials**

**B - The user decides what kind of Gaia-X Compliant Verifiable Credential to obtain from the ones made available by Gaia-X.** 

E.g.: LegalParticipant.

The list of available VCs can be retrieved from the Gaia-X Registry, using the /v1/api/trusted-shape-registry/v1/shapes/implemented endpoint.

**C - The user decides the method to obtain the compliance**

1.	Through the Gaia-X Wizard: https://wizard.lab.gaia-x.eu/

2.	Through direct API calls: https://compliance.gaia-x.eu/

Please note that third-party applications might also be integrated with the Gaia-X Compliance, but they are out of scope for this guide.

**D - The user creates first their credential payload**

Users create the payload with the mandatory attributes as well as optional attributes needed in their ecosystem. 
The mandatory attributes vary depending on the type of VC, and the full list of mandatory attributes can be retrieved from the Gaia-X Registry, using the /v1/api/trusted-shape-registry/v1/shapes endpoint.

**E - The user signs their credentials with their private key**

The https://wizard.lab.gaia-x.eu/ can also be used for this step, but the user is free to choose their preferred signing tool.

**F - The user creates a Verifiable Presentation** 

The Verifiable Presentation includes all the Verifiable Credentials that are required to get the compliance for their Participant or their service. The https://wizard.lab.gaia-x.eu/ can also be used for this step, but the user is free to choose the tool of their choice.

**G - The user calls the Gaia-X Compliance Service for their presentation**

The Gaia-X Compliance Service is connected in the background with the available Clearing Houses, and the call will go to one of the GXDCH instances. But the experience is seamless for the user.
The https://wizard.lab.gaia-x.eu/ can also be used for this step, but the user is free to choose their preferred tool. A direct API call is also possible.
If a user wishes to use a specific clearing house instance, this option is available from:

1. the https://wizard.lab.gaia-x.eu/ by selecting a specific Clearing House from the drop-down menu.

2. calling directly the API of the clearing house. More information on how to obtain that can be found in the GXDCH documentation.

**H1 - If the verification fails, an error message will be returned to the user to identify the issue**

**H2 - If the verification is successful, the user will receive a Gaia-X Verifiable Credential.**

The Gaia-X Verifiable Credential contains the proof of the verification, signed by the Clearing House which did the verification.

After having received the Gaia-X Verifiable Credential one can claim they are a Gaia-X Conformant Legal Participant, or have Gaia-X Conformant Services based on the proof in the VC returned by the Compliance Service.

The Gaia-X Conformant VCs can be:

1.  stored in JSON file format saved on the user's device.

2.	stored in a digital wallet.

3.	pushed to the Credential Event Service. This service is the base of the creation of Federated Catalogues.
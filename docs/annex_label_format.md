# Gaia-X Label format

In case of a valid verification and validation of the criteria for a specific assessment scheme, the accredited Gaia-X Compliance services are issuing a Gaia-X Label.

A Gaia-X Label is a machine readable, structured and signed document that comprises at a minimum the following information attributes:

-	Label ID, as unique identifier for the label being issued for a specific Service Offering.
-	Participant ID, as unique identifier for the Participant that is awarded the Label.
-	Participant Business ID, presenting the firm business ID of the Participant.
-	Service Offering for which the Label is applicable.
-	Conformity assessment scheme, as Gaia-X Standard Compliance, Gaia-X Label Level 1, Gaia-X Label Level 2 or Gaia-X Label Level 3.
-	Reference to the assessment scheme version that comprised the Gaia-X criteria for which the claims and evidences were prepared.
-	Compliance Service ID, as unique identifier for the Compliance Service that issued the Label.
-	Compliance Service version, as the software version that issued the Label.
-	Issuance date on which the Label was issued.
-	Validity start and end date on which the Label will expire.


!!!tip
    In technical terms, a Gaia-X Label is a [W3C Verifiable Credential](https://www.w3.org/TR/vc-data-model-2.0/) and the JSON schema covering the above functional expectations is available via the [Gaia-X Registry](https://registry.gaia-x.eu/).
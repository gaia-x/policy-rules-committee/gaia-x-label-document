# Services and Resources - Mandatory attributes

The following defines the mandatory attributes to be provided in the Gaia-X Credentials to describe Services and Resources. 

## Service & Subclasses

### Service offering

This is the generic format for all service offerings:

| Attribute              | Card. | Trust Anchor | Comment                                         |
|------------------------|-------|--------------|-------------------------------------------------|
| `providedBy`           | 1     | Gaia-X Registry        | a resolvable link to the `participant` self-description providing the service    |
| `termsAndConditions[]` | 1..*  | Gaia-X Registry       | a resolvable link to the Terms and Conditions applying to that service. |
| `policy[]`             | 1..*  | Gaia-X Registry        | a list of `policy` expressed using a DSL (e.g., Rego or ODRL) (access control, throttling, usage, retention, ...) |
| `dataAccountExport[]`   | 1..*  | Gaia-X Registry       | list of methods to export data from your user's account out of the service |

**termsAndConditions structure**

| Attribute              | Card. | Trust Anchor | Comment                                         |
|------------------------|-------|--------------|-------------------------------------------------|
| `URL`                  | 1     | Gaia-X Registry        | a resolvable link to document    |
| `hash`                 | 1     | Gaia-X Registry        | sha256 hash of the above document. |

**dataAccountExport structure**

The purpose is to enable the participant ordering the service to assess the feasibility of exporting its personal and non-personal data out of the service.  
This export shall cover account data e.g., account holder's billing information, information on the PII held - but also data provided previously to the service by the user.

| Attribute     | Card. | Trust Anchor | Comment                                         |
|---------------|-------|--------------|-------------------------------------------------|
| `requestType` | 1     | Gaia-X Registry        | the mean to request data retrieval: `API`, `email`, `webform`, <br> `unregisteredLetter`, `registeredLetter`, `supportCenter` |
| `accessType`  | 1     | Gaia-X Registry        | type of data support: `digital`, `physical` |
| `formatType`  | 1     | Gaia-X Registry        | type of Media Types (formerly known as MIME types) as defined by the [IANA](https://www.iana.org/assignments/media-types/media-types.xhtml). |

**Consistency rules**

- the keys used to sign a SERVICE OFFERING description and the `providedBy` PARTICIPANT description should be from the same keychain.

### Service Instance / Instantiated Virtual Resource

An Instantiated Virtual resource is a running resource exposing endpoints such as, and not limited to, a running process, an online API, a network connection, a virtual machine, a container, an operating system. 

| Attribute            | Card. | Trust Anchor | Comment                                   |
|----------------------|-------|--------------|-------------------------------------------|
| `maintainedBy[]`     | 1..*  | Gaia-X Registry        | a list of `participant` maintaining the resource in operational condition. |
| `hostedOn`           | 1     | Gaia-X Registry       | a `resource` where the process is located (physical server, datacenter, availability zone, ...). |
| `instanceOf`         | 1     | Gaia-X Registry       | a `virtual resource` (normally a `software` resource) this process is an instance of. |
| `tenantOwnedBy[]`    | 1..*  | Gaia-X Registry       | a list of `participant` with contractual relation with the resource. |
| `serviceAccessPoint[]`| 1..*  | Gaia-X Registry        | a list of [Service Access Point](https://en.wikipedia.org/wiki/Service_Access_Point) which can be an endpoint as a mean to access and interact with the resource |


## Resource & Subclasses

### Physical Resource
 
A Physical resource is, but is not limited to, a datacenter, a bare-metal service, a warehouse, or a plant. Those are entities that have a weight and position in physical space.

| Attribute              | Card. | Trust Anchor | Comment                                     |
|------------------------|-------|--------------|---------------------------------------------|
| `maintainedBy[]`       | 1..*  | Gaia-X Registry        | a list of `participant` maintaining the resource in operational condition and thus having physical access to it. |
| `locationAddress[].countryCode` | 1..*  | Gaia-X Registry  | a list of physical locations in ISO 3166-2 alpha2, alpha-3 or numeric format. |

**Sustainability**

The following attribute has to be linked to a service offering, physical resource or legal person.  
The aim of the attribute is to allow a customer more educated and transparent decisions regarding the environmental impact.

| Attribute              | Card. | Trust Anchor | Comment                                     |
|------------------------|-------|--------------|---------------------------------------------|
| `environmentalImpactReport`       | 1  | `ownedBy` or `providedBy`         |a resolvable link to an environmental impact report of the Provider or a link to a public database listing the organisation as a signatory of a public engagement to reach Climate Neutrality by 2030 (e.g. Climate Neutral Data Centre Pact - [signatories](https://www.climateneutraldatacentre.net/signatories/)). |


### Virtual Resource

A Virtual resource is a resource describing recorded information such as, but not limited to, a dataset, software, a configuration file, and an AI model.

| Attribute            | Card. | Trust Anchor | Comment                                   |
|----------------------|-------|--------------|-------------------------------------------|
| `copyrightOwnedBy[]` | 1..*  | Gaia-X Registry        | A list of copyright owners either as a free-form string or `participant` URIs from which Self-Descriptions can be retrieved. A copyright owner is a person or organization that has the right to exploit the resource. The copyright owner does not necessarily refer to the author of the resource, who is a natural person and may differ from the copyright owner. |
| `license[]`          | 1..*  | Gaia-X Registry        | A list of [SPDX](https://github.com/spdx/license-list-data/tree/master/jsonld) identifiers or URL to document|
| `policy[]`           | 1..*  | Gaia-X Registry        | a list of `policy` expressed using a DSL (e.g., Rego or ODRL) (access control, throttling, usage, retention, ...) |

The `license` refers to the license of the virtual resource - data or software, not the license of a potential instance of that virtual resource.

If there are no specified usage policy constraints on the `VirtualResource`, the `policy` should express a simple `default: allow` intent.
# Gaia-X Digital Clearing House

## The What

GXDCH = Operationalise the Gaia-X Framework and acts as Gaia-X label issuer for compliant levels

1.	The GXDCH is a node of validation and verification of the Gaia-X rules;
2.	It is the go-to place to obtain Gaia-X compliance and become part of the Gaia-X ecosystem;
3.	The GXDCH are non-exclusive, interchangeable multiple nodes, operated by market operators, acting on behalf Gaia-X;
4.	GXDCH operate and run services of the Gaia-X Framework necessary to achieve compliance and support the onboarding of any Gaia-X adopter;
5.	They integrate to external TA (Trust Anchors), including CAB (Conformance Assessment Bodies) for external asseverations, Identity Verification (like eIDAS), and other TDS (Trusted Data Sources) as defined by the AISBL.




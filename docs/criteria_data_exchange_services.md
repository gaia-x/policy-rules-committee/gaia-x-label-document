# Gaia-X Compliance Criteria for Data Exchange Services

In Gaia-X, `Data` is at the core of Data Exchange Services. `Data` are furnished by `Data Producers` (for instance data owners or data controllers in the GDPR sense, data holder in EU data acts sense, etc.) to `Data Product Providers` who compose these data into a `Data Product` to be used by `Data Consumers`.

Further details of all the terms used in this section are defined in the section on Data Exchange Services of the Gaia-X Architecture Document and in the Data Exchange Services specifications, and to keep definitions consistent across documents and versions, they won't be duplicated here.

<script src="../js/fetchJSONcriteria.js"></script>

???tip "Criteria extract in JSON"
    A machine readable version of the criteria in JSON is available here. <a><button onclick="fetchJSONcriteria()">Download JSON</button></a>

## Criteria


```criteria
id: D1.1.1
criterion: The Data Product shall be a Gaia-X compliant Service Offering.
standard_compliance: declaration
labelL1: declaration
labelL2: declaration
labelL3: declaration
declaration: See [P.1.2.9](../criterions/#P1.2.9)
```

```criteria
id: D1.1.2a
criterion: The `Data Product Provider` offering the `Data Product` shall be a Gaia-X Participant.
standard_compliance: declaration
labelL1: declaration
labelL2: declaration
labelL3: declaration
```

```criteria
id: D1.1.2b
criterion: The `Data Product Provider` shall deliver the `Data Product` only to `Data Consumers` with a Gaia-X compliant description
standard_compliance: declaration
labelL1: declaration
labelL2: certification
labelL3: certification
permissible_standards:
- "a conformity assessment scheme which includes the verification of records in the Data Usage Logging Service"
```

!!!note
    This criterion is important to create trust at the data licensor/data producer level.

```criteria
id: D1.1.3
criterion: For each `Data Product`, the `Data Product Provider` shall have the legal authorization from the `Data Producer`(s) to include the data in the `Data Product`.
standard_compliance: declaration
labelL1: declaration
labelL2: certification
labelL3: certification
declaration: The Data Product Description shall include links to authorization documents which are signed through a Gaia-X authorized Trust Service Provider.
permissible_standards:
- "a conformity assessment scheme which includes the verification that the authorization documents are legally valid."
```

<!--
from the list of mandatory information in the PRCD 23.10:
- For each Data Product, the information on the credential of the participant legally enabling the data usage shall be provided.
-->

!!!note
    If the data product aggregates data from several data producers, then the data product provider shall have a legal authorization from each data producer.

!!!note
    The legal authorization will often be subordinated to the data usage agreement from the data licensor(s).
    Indeed the Data Product will usually be generic (e.g. customer banking transactions) and the real scope (e.g. Jane Doe’s transactions) will be defined during instantiation before data usage.

```criteria
id: D1.1.4
criterion: For each `Data Product`, the `Data Product Provider` shall provide in the `Data Product Description` a `Data License` defining the usage policy in ODRL for all data in this Data Product.
standard_compliance: declaration
labelL1: declaration
labelL2: declaration
labelL3: declaration
declaration: >
  The Data Product Description shall include a data license expressed as a valid ODRL document containing at least indication that the data product contains or not licensed data and, in that case, the template of the Data Usage Agreement to be signed by the data licensor(s) before data usage.
  The Data license shall contain:

    1. the constraints specific to the Data Product Provider.
    2. indication that the data product contains or not licensed data and in that case.
    3. the template of the Data Usage Agreement to be signed by the data licensor(s) before data usage.
```

<!--
from the list of mandatory information in the PRCD 23.10:
- For each Data Product, it shall be specified whether it contains licensed data (in particular, but not only, Personal Identifiable Information as per GDPR).
- The description of a Data Product can only be made by its Data Product Provider.
- If Data Producer or Data Licensor are specified, they can be the only participants describing the consent on a Data Product.
-->

```criteria
id: D1.1.5
criterion: The `Data Product Provider` shall deliver the `Data Usage`, instantiating the `Data Product`, only to `Data Consumer(s)` which have formally accepted the `Data Product Usage Contract`.
standard_compliance: declaration
labelL1: declaration
labelL2: certification
labelL3: certification
declaration: Yes/No
permissible_standards:
- "a conformity assessment scheme which includes performing correlation of the records in the Data Usage Logging Service with the Data Product Usage Contracts (either provided by the Data Product Provider or through a Data Product Usage Contract Store) and verifying that each contract is formally accepted by the Data Consumer."
```

!!!note
    A `Data Product Usage Contract` is a Ricardian contract: a contract at law that is both human-readable and machine-readable, cryptographically signed and rendered tamper-proof. 

!!!note
    A Data Consumer can formally accept the Data Product Usage Contract either through a qualified digital signature or through a record from a Gaia-X Trusted Source (e.g. trusted data intermediary)

```criteria
id: D1.1.6a
criterion: For each licensed data element included in the `Data Product`, the `Data Product Provider` shall ensure that each `Data Product Usage Contract` includes `Data Usage Agreement(s)` (DUA) provided by the `Data Licensor(s)` explicitly authorizing the `Data Usage` by the `Data Consumer`.
standard_compliance: declaration
labelL1: declaration
labelL2: certification
labelL3: certification
declaration: In case of data liable to EU regulations (GDPR, EU acts on data ...), the provided `Data Usage Agreement` must contain all information required by the regulation (e.g. consent as per GDPR, authorizations/permissions as per EU acts on data, permissions as per the EU Finance Data Access regulation, etc...).
permissible_standards:
- "a conformity assessment scheme which includes the verification that the `Data Product Usage Contracts` contain appropriate `Data Usage Agreement(s)`"
```

<!--
from the list of mandatory information in the PRCD 23.10:*
- If the Data Product contains licensed data, then the Data Product Usage Contract shall include an explicit Data Usage Agreement signed by the Data Licensor.
-->

!!!note
    A `Data Licensor` is a natural or legal `Participant` who owns usage rights for some `Data`. It can be a data subject as per GDPR for personal data or a primary owner of non-personal data (i.e. not liable to GDPR).

!!!note
    The `Data Licensor(s)` can provide the `Data Usage Agreement(s)` either through a qualified digital signature or through a record from a Gaia-X Trusted Source (e.g. a trusted data intermediary).

!!!note
    The `Data Usage Agreement(s)` gives the `Data Product Provider` the legal authorization for providing the data to the `Data Consumer`. 
    The DUA contains usage terms and conditions associated with these data (permissions, prohibitions, duties ...).

!!!note
    Controlling that the `Data Licensor` is legally authorized to give a `Data Usage Agreement` is often domain specific (for instance a farmer can give agreement to use data related to a parcel only if she/he owns or rents this parcel). 

```criteria
id: D1.1.6b
criterion: The `Data Product Provider` shall deliver the `Data Usage` instantiating the `Data Product` only to `Data Consumer(s)` which fulfill the constraints in the `Data Usage Agreements`.
standard_compliance: declaration
labelL1: declaration
labelL2: certification
labelL3: certification
declaration: Yes/No
permissible_standards:
- "a conformity assessment scheme which includes the verification checking that each `Data Consumer` of the `Data Product` has provided appropriate Verifiable Credentials for the constraints in the `Data Usage Agreements`"
```

<!--
from the list of mandatory information in the PRCD 23.10 - to be analyzed and possibly included:
- For each Data Product, the information on the data exchange component that exposes the data resource shall be provided.
- For Data Exchange components, the description of the Service offering shall contain or point to an Instantiated Virtual Resource with at least one Service Access Point being a Contract Negotiation Endpoint.
- For Data Exchange components, the output of the Contract Negotiation Endpoint shall point to the result of the negotiation signed by all the Participants in direct link with the negotiation.
-->

!!!note
    Controlling that the `Data Consumer` fulfils the constraints expressed in the `Data Usage Agreement(s)` is often domain specific (for instance a patient might agree to share medical data to non-profit research laboratories from specific countries with defined cyber-security certificates).
    A generic way to implement this criterion is to request the `Data Consumer` to provide, in the `Data Product Usage Contract`, the appropriate Verifiable Credentials issued by Gaia-X Trusted Data Sources. 














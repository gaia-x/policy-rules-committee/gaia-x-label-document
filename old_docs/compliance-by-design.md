# Compliance by Design
** MappingCompliance by Design requirements & Labelling Criteria **

# Introduction

## BACKGROUND

In May 2022, a working group of the Gaia-X Banking
and Insurance Dataspace created the document “Compliance by Design”
which is based on regulatory requirements of the financial service
industry. The document addresses the most critical legal and
regulatory challenges in the use of public cloud technologies by
highly regulated industries. The aim is to support the building of a
trusted cloud by Gaia-X. The document "Compliance by Design"
is therefore explicitly addressed to the working groups on policy
rules and labelling for the future mapping of the minimum standards
of regulated industries.

The requirements of the General Data Protection
Regulation (GDPR), the Digital Operational resilience Act (DORA), the
EBA revised Guidelines on outsourcing arrangements, the Directive on
Security of Network and information Systems (NIS Directive), the
Cybersecurity act and other requirements serve as the basis for this.
However, it should be noted that the requirements documented so far
in the document "Compliance by Design V2.0" only include
the most urgent aspects and are not conclusive.

Especially the industry agnostic requirements from
DORA play an important role in the design of the certification, the
so-called labelling in Gaia-X.

## AGREED WORKING PROCESS

On September 14, 2022,
the above-mentioned legal task force took part in the meeting
organized in Brussels by the Gaia-X LABEL and Policy Rules Team to
explain the need to open a legal section as part of the development
of the criterion retained for the constitution of Labels.
It was agreed at the end of the meeting
to provide a document underlying the legal expectations regarding
cloud outsourcing which are already mapped to the related Criteria.
We have chosen to work by identifying themes
to which criteria correspond and not an
evaluation by criterion.

In the following chapters, the prototype is worked
out using the theme. "Assignment of
Sub-Contracting/Sub-processing"
(also section 4 from the document "Compliance by Design V2.0").
A reference is made to the existing criteria from the Policy Rules
and labelling document (Ch1) and legal expectations from Users  base
on regulatory requirements (ch2 non exhaustive). The task
force Compliance by Design, has been
extended since 19.09.2022 by Users from
others sectors, members of the CIGREF1,
who joined the written request (below) to
integrate the legal dimension into the content of the criteria.

## AIM OF THE DOCUMENT

Based on this topic, we can align the joint work
with the given criteria, which leads to the addition of legal aspects
and the detail of the technical requirements of each criterion. In
addition, the goal is to make the PRLD the central document for all
questions related to labelling. Either with the help of concrete
references to applicable law or binding regulation or by
supplementing references to relevant documentation within Gaia-X.
	
# IDENTIFIED CRITERION FROM GAIA-X RELATED TO THE SUBCONTRACTING ISSUE


## Criterion10:

The provider shall explain how information about subcontractors and
related data localisation will be communicated

## Criterion 11:

The provider shall communicate to the customer where the applicable
jurisdiction(s) of subcontractors will be

## Criterion 26:

The provider shall clearly define if and to which extent sub-processors
will be involved

## Criterion 27:

The provider shall clearly define if and to the extent sub-processors
will be involved, and the measures that are in place regarding
sub-processors management

## Criterion 56:

For label level 3, where the provider or subcontractor is subject to
legal obligations to transmit or disclose data based on a non-EU/EEA
statutory order, the provider shall have verified safeguards in place
to ensure that any access request is compliant with EU/EEA/Member
State law

## Criterion 59:

For label level 3, in the event of recourse by the provider, in the
context of the services provided to the customer, to the services of
a third-party company -including a subcontractor -whose registered
head office, headquarters and main establishment is outside of the
European Union or who is owned or controlled directly or indirectly
by another third-party company registered outside the EU/EEA, the
third-party company shall have no access over the customer data nor
access and identity management for the services provided to the
customer. The provider, including any of its sub-processor, shall
push back any request received from non-European authorities to
obtain communication of personal data relating to European customers,
except if request is made in execution of a court judgment or order
that is valid and compliant under Union law and applicable member
states law as provided by Article 48 GDPR

# LEGAL EXPECTATIONS FROM USERS :

## Linked to Criteria 10.11.26.27/ Legal contractual framework must include at least the following commitments below:

- Definition of « subcontracting contract » according to the Member state Law
of the User in order to be agreed on who is qualified as a «
subcontractor »
	
- Accurate Identification of subcontractors (name/head office located /
nationality of the registered company/ nationality of the major
shareholder…) involved in the relation between the CSP and the
user shall be mentioned in the contract
	
- The nature of the activities subcontracted shall be described and the
related data process (for which services, on which data, where when
and how long) by subcontractor shall be identified in the contract 
	
- CSP must inform in written the user of any new subcontractors involved
during the life of the contract and give the all the new
subcontractors details required
	
- The provider shall not engage with any subcontractor without the prior
written consent of the Bank at any time during the life of the
contract. The Bank shall have a 3-month period from the reception of
the information sent by CSP to give their consent corresponding to a
risk assessment period.


## Linked to Criteria 56 /Legal contractual framework must include at least the following commitments below:

	
- Provider commits to impose to the subcontractor in their sub-contractual
relationship that the subcontractor complies with all applicable
laws, regulatory requirements and contractual obligations linked to
the main contract between the provider and the user.
	
- Providermust guarantee in the subsequent subcontracts that it has entered into:


- The postponement of the contractual obligations provided for in the
service contract signed between the client and him;

- The organization of the contractual liability of subcontractors 
vis-à-vis him and the client (security of data and processing,
confidentiality, etc.) and in particular in terms of audits:
	
		
	- Ensure that the subcontractor has an audit procedure;
		
	- Systematically provide the first page of each audit report to the client;
		
	- Justify regular meetings with its subcontractors to ensure that the
	procedures are respected and applied;
		
	- Follow the analyses and results of the audits (follow-up of deviations,
	follow-up of actions implemented, etc.).
	
	
- Providers shall disclose upon the request to the user the extracted provisions
from the subcontract related to the subcontractor’s commitment.



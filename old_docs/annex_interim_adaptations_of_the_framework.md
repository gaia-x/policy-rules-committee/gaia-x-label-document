# Scope of this document #
Gaia-X is striving to become operational in due time.
The Labelling Document (Operational Conditions) outlines the visionary anticipated process of how Gaia-X labels shall be issued.
Gaia-X acknowledges that the highly sophisticated approaches, especially in regards of alignment of a multitude of existing third-party standards and fully automatising the issuance may not be implementable for the time being. 
Therefore, Gaia-X will define deviating, interim provisions, allowing the operationalization of the Gaia-X Labels in due time whilst enhancing the processes and seeking for overarching alignment of relevant standards to facilitate and enable the visionary approach of Gaia-X.

# List of deviating approaches #
## Meta-Approach and automatic decisions ##
Given the meta-approach of the Gaia-X Label, the several building blocks submitted to the Label Issuer must match. 
Currently, Gaia-X refers to third-party standards that provide attestations that are not distinctively related to a Service Offering. 
Gaia-X will have to determined the process on how to validate that the provided attestations actually apply to the Service Offering for which they will be submitted. 
Even where third-party standards already apply to a service, given the multitude of third-party standards, the references might still not match. Gaia-X will have to determine the process on how to validate that despite of different references, the attestations do apply to the Service Offering for which such attestations were submitted. 

## tbc ##

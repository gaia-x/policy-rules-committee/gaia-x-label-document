import yaml
from pydantic import BaseModel, Field
from typing import Optional, List
from enum import Enum
import markdown
import html
import jinja2

## DOC: read the code from https://github.com/facelessuser/pymdown-extensions/blob/main/pymdownx/superfences.py


TEMPLATE_MD = """
---

<a id="{{ criteria.id }}" href="#{{ criteria.id }}"><b>Criterion {{ criteria.id }}</b></a>: {{ criteria.criterion }}

| Standard Compliance                    | Label Level 1              | Label Level 2              | Label Level 3              |
|----------------------------------------|----------------------------|----------------------------|----------------------------|
|{{ criteria.standard_compliance.value }}|{{ criteria.labelL1.value }}|{{ criteria.labelL2.value }}|{{ criteria.labelL3.value }}|

**Declaration**: {{ criteria.declaration }}

**Permissible Standards**:{% if criteria.permissible_standards %}

{% for standard in criteria.permissible_standards -%}
  - {{ standard|e }}
{% endfor -%}{% else -%}N/A{% endif %}

**Example Standards**:{% if criteria.example_standards %}

{% for standard in criteria.example_standards -%}
  - {{ standard|e }}
{% endfor -%}{% else %}N/A{% endif %}

{% if criteria.point_of_reference_standards %}
**Point Of Reference Standards**:

{% for standard in criteria.point_of_reference_standards -%}
  - {{ standard|e }}
{% endfor -%}
{% endif %}
"""

TEMPLATE_HTML = """
<span hidden class="criteria-data">{{ criteria.model_dump_json() }}</span>
"""

class AttestationEnum(str, Enum):
    declaration = 'declaration'
    certification = 'certification'
    NA = 'N/A'

class Criteria(BaseModel):
    id: str
    criterion: str
    standard_compliance: AttestationEnum = Field(default=AttestationEnum.NA)
    labelL1: AttestationEnum = Field(default=AttestationEnum.NA)
    labelL2: AttestationEnum = Field(default=AttestationEnum.NA)
    labelL3: AttestationEnum = Field(default=AttestationEnum.NA)
    declaration: str = Field(default="N/A")
    permissible_standards: List[str] = Field(default=[])
    example_standards: List[str] = Field(default=[])
    point_of_reference_standards: List[str] = Field(default=[])

def _format(source, language, class_name, options, _md, **kwargs):
    environment = jinja2.Environment()
    raw_criteria = yaml.safe_load(source)
    criteria = Criteria(**raw_criteria)
    # TODO: no idea why I can't use the _md argument to process markdown. Only the last block is processed.
    # FIX: create a local instance for each block but waste of resource.
    md = markdown.Markdown(extensions=[
                "nl2br",
                "footnotes",
                "attr_list",
                "smarty",
                "sane_lists",
                "tables",
                "admonition",
            ])
    md_doc = environment.from_string(TEMPLATE_MD).render(criteria=criteria)
    html_doc = md.convert(md_doc)
    html_doc += environment.from_string(TEMPLATE_HTML).render(criteria=criteria)
    return html_doc

def format(source, language, class_name, options, md, **kwargs):
    """Format criteria block."""
    try:
        assert language == 'criteria'
        assert class_name == 'criteria'
        return _format(source, language, class_name, options, md, **kwargs)
    except Exception as ex:
        return '<pre><code>%s</code></pre>' % html.escape(str(ex))

import markdown
import block

FIXTURES = [
"""id: P1.1.1
criterion: The Provider shall offer the ability to establish a legally binding act.
  This legally binding act shall be documented.
conformity: declaration
l1: declaration
l2: declaration
l3: declaration
declaration: Using the Gaia-X Ontology, the declaration shall contain either a resolvable identifier pointing to the legally binding act offered by the Provider or a contact form to request more information.
permissible_standards:
- "SecNumCloud: 19.1"
- "BSI C5: BC-01, OIS-03"
- "CISPE (GDPR, Infrastructure & IaaS): 4.2"
- "EU Cloud CoC (GDPR, XaaS): 5.1.A, 5.1.B"
- "CSA CCM: STA-09"
- "SWIPO IaaS: FR1, FR2"
""",
"""id: P1.1.2
criterion: The Provider shall offer the ability to establish a legally binding act.
declaration: "**test**"
conformity: declaration
""",
"""id: P1.1.3
criterion: The Provider shall offer the ability to establish a legally binding act.
declaration: >
  The declaration shall include at least one of the following:

    1. Detailed description of the parties using the Gaia-X Ontology.
    2. Use of legally relevant or legally binding cryptographic certificates from the Gaia-X Registry (note: this is not applicable in case of manual signature).
"""
]

if __name__ == "__main__":
    md = markdown.Markdown(extensions=[
                "nl2br",
                "footnotes",
                "attr_list",
                "smarty",
                "sane_lists",
                "tables",
                "admonition",
            ])
    kwargs = {'classes': [], 'id_value': '', 'attrs': {}}
  
    for idx, fixture in enumerate(FIXTURES):
        print("*" * 10, idx, "*" * 10)
        print(block.format(fixture, 'criteria', 'criteria', {}, md, **kwargs))
